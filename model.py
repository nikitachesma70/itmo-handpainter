import mediapipe as mp
from mediapipe.tasks import python
from mediapipe.tasks.python import vision
import cv2
from abc import ABC, abstractmethod
import numpy as np
import torch
import albumentations as A
from albumentations.pytorch import ToTensorV2
import timm
import torch.nn as nn
from palm_detector import PalmDetection


class BaseModel(ABC):
    @abstractmethod
    def get_forefinger_coords_from_image(image) -> list:
        pass


class MediaPipeModel(BaseModel):
    def __init__(self, path_to_model: str) -> None:
        # STEP 2: Create an HandLandmarker object.
        base_options = python.BaseOptions(model_asset_path=path_to_model)
        options = vision.HandLandmarkerOptions(base_options=base_options, num_hands=2)
        self.detector = vision.HandLandmarker.create_from_options(options)

    def get_forefinger_coords_from_image(self, image: np.array) -> list:
        image = image.copy()
        h, w, c = image.shape
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = mp.Image(image_format=mp.ImageFormat.SRGB, data=image)

        detection_result = self.detector.detect(image)
        image = image.numpy_view()
        if len(detection_result.handedness):
            forefinger = detection_result.hand_landmarks[0][8]
            x = int(forefinger.x * w)
            y = int(forefinger.y * h)
            return [x, y]
        return []


class MobileNetV2_050(BaseModel):
    def __init__(self, path_to_model: str, device: str, is_debug: bool) -> None:
        self.preprocessing = A.Compose([A.Resize(height=224, width=224), A.Normalize(), ToTensorV2()])
        self.device = device
        self.init_model(path_to_model)
        self.palm_detector = PalmDetection()
        self.is_debug = is_debug

    def init_model(self, path_to_model):
        self.model = timm.create_model("mobilenetv2_050", pretrained=True, num_classes=0)
        self.model.global_pool = nn.Flatten()
        self.model.classifier = nn.Sequential(nn.Linear(62720, 42), nn.Sigmoid())
        self.model.load_state_dict(torch.load(path_to_model))
        self.model.to(self.device)
        self.model.eval()

    def detect_palm(self, image: np.array) -> list:
        hands = self.palm_detector(image)

        cap_height, cap_width, c = image.shape
        wh_ratio = cap_width / cap_height
        rects = []

        if len(hands) > 0:
            for hand in hands:
                sqn_rr_size = hand[0]
                sqn_rr_center_x = hand[2]
                sqn_rr_center_y = hand[3]

                cx = int(sqn_rr_center_x * cap_width)
                cy = int(sqn_rr_center_y * cap_height)
                xmin = int((sqn_rr_center_x - (sqn_rr_size / 2)) * cap_width)
                xmax = int((sqn_rr_center_x + (sqn_rr_size / 2)) * cap_width)
                ymin = int((sqn_rr_center_y - (sqn_rr_size * wh_ratio / 2)) * cap_height)
                ymax = int((sqn_rr_center_y + (sqn_rr_size * wh_ratio / 2)) * cap_height)
                xmin = max(0, xmin)
                xmax = min(cap_width, xmax)
                ymin = max(0, ymin)
                ymax = min(cap_height, ymax)
                rects.append([cx, cy, (xmax - xmin), (ymax - ymin)])

            rects = np.asarray(rects, dtype=np.float32)
            for rect in rects:
                rcx = int(rect[0])
                rcy = int(rect[1])
                half_w = int(rect[2] // 2)
                half_h = int(rect[3] // 2)
                x1 = rcx - half_w
                y1 = rcy - half_h
                x2 = rcx + half_w
                y2 = rcy + half_h
                if x1 > 0 and x2 > 0 and y1 > 0 and y2 > 0:
                    return y1, y2, x1, x2
        return []

    def get_forefinger_coords_from_image(self, image: np.array) -> list:
        image = image.copy()
        h, w, c = image.shape
        hands_coord = self.detect_palm(image)
        if len(hands_coord):
            y1, y2, x1, x2 = hands_coord
            palm_image = image[y1:y2, x1:x2].copy()

            pred = self.model(self.preprocessing(image=palm_image)["image"].unsqueeze(0).to(self.device))
            pred = pred.cpu().detach().numpy()[0]

            if self.is_debug:
                for i in range(0, len(pred), 2):
                    x = int(pred[i] * (x2 - x1))
                    y = int(pred[i + 1] * (y2 - y1))
                    palm_image = cv2.circle(palm_image, (x, y), 2, (0, 255, 0), -1)
                cv2.imshow("1234", palm_image)

            # plt.imshow(image)
            if len(pred):
                x = x1 + int(pred[16] * (x2 - x1))
                y = y1 + int(pred[17] * (y2 - y1))
                return [x, y]
        return []
