import cv2
from model import MediaPipeModel, MobileNetV2_050


colors_dict = {1: (255, 0, 0), 2: (0, 255, 0), 3: (0, 0, 255)}


def draw_trajectory(image, trajectory_list):
    image = image.copy()
    for point in range(len(trajectory_list) - 1):
        # if point + 1 < len(trajectory_list):
        cv2.line(
            image,
            [*trajectory_list[point][0]],
            [*trajectory_list[point + 1][0]],
            colors_dict[trajectory_list[point + 1][1]],
            4,
        )
    return image


def draw_instruction(image, color):
    image = image.copy()
    font = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 1
    color = colors_dict[color]
    thickness = 2
    image = cv2.putText(image, "1-3: Change color", (10, 30), font, fontScale, color, thickness, cv2.LINE_AA)
    image = cv2.putText(image, "4-5: Change model", (10, 60), font, fontScale, color, thickness, cv2.LINE_AA)

    return image


if __name__ == "__main__":
    model = MediaPipeModel("models/hand_landmarker.task")
    trajectory_list = []
    draw_color = 1
    cap = cv2.VideoCapture(0)
    ret, image = cap.read()

    h, w, c = image.shape
    count_draw_points = 10
    while True:
        ret, image = cap.read()
        if ret:
            image = cv2.flip(image, 1)
            forefinger = model.get_forefinger_coords_from_image(image)
            if len(forefinger):
                trajectory_list.append([forefinger, draw_color])
                # image = draw_landmarks_on_image(image, detection_result)
            image = draw_trajectory(image, trajectory_list)
            image = draw_instruction(image, draw_color)
            cv2.imshow("HandPainter", image)
            if len(trajectory_list) > count_draw_points:
                trajectory_list.pop(0)
        key = cv2.waitKey(1)
        if key == ord("q"):
            break
        elif ord("1") <= key <= ord("3"):
            draw_color = key - 48
        elif ord("4") == key:
            model = MediaPipeModel("models/hand_landmarker.task")
        elif ord("5") == key:
            model = MobileNetV2_050("models/mobilenetv2_050_33ep.ckpt", "cuda", is_debug=False)
