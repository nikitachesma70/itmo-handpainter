import os
import glob
import cv2
import numpy as np
from utils import load_db_annotation, read_img, colors, bones, projectPoints

# PATH_TO_IMG = "dataset/FreiHAND/FreiHAND_pub_v2/training/rgb"
PATH_TO_IMG = "dataset/FreiHAND/FreiHAND_pub_v2"
PATH_TO_ANNOTATION = "dataset/FreiHAND/FreiHAND_pub_v2/"


db_data_anno = load_db_annotation(PATH_TO_ANNOTATION, "training")

img_list = glob.glob(os.path.join(PATH_TO_IMG, "*.jpg"))

# for idx, (img_path, annotation) in enumerate(zip(img_list, db_data_anno)):
for idx, annotation in enumerate(db_data_anno):
    # image = cv2.imread(img_path)
    image = read_img(idx, PATH_TO_IMG, "training", "auto")
    w, h, c = image.shape
    K, mano, xyz = annotation
    uv = projectPoints(xyz, K)
    # for keypoint in uv:
    for connection, color in bones:
        coord1 = list(map(int, uv[connection[0], :]))
        coord2 = list(map(int, uv[connection[1], :]))
        coords = np.stack([coord1, coord2])

        cv2.line(image, coord1, coord2, color * 255, 2)
    for i in range(21):
        cv2.circle(image, list(map(int, uv[i])), 3, colors[i] * 255, -11)

    image = cv2.resize(image, (1000, 1000))
    cv2.imshow("image", image)
    if cv2.waitKey(0) == ord("q"):
        exit(0)

print(0)
